import logging


def getFileLogger():
    """Returns the logger that writes to a file
    """

    logger = logging.getLogger('techtrends')
    hdlr = logging.FileHandler('techtrends.log')
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    return logger
