/**
 * Checks if console and console.log are available - if not,
 * we append an empty function, so we don't cause exceptions.
 */
if(console === undefined || console.log === undefined) {
	
	var console = {};
	
	console.log = function() {
		return;
	};
}

/**
 * Put everything in this function to have all in
 * one single scope!
 * 
 * Note: This function is also used in techtopics.js!
 */
function techtrends() {

	/**
	 * This closure initializes all datepickers!
	 */
	(function() {

	    var today = new Date(); // today
	    
	    var past = new Date();
	    past.setMonth(today.getMonth()-2); // 3 months ago
	
	    // if a month/day is < 10, its string has only one digit, e.g. "2". But
	    // we want a string like "02", so we put a "0" in front of it.
	    var today_y = today.getFullYear();
	    var today_m = (today.getMonth()+1) < 10? "0" + (today.getMonth()+1) : (today.getMonth()+1);
	    var today_d = today.getDate() < 10? "0" + today.getDate() : today.getDate();
	    
	    var past_y = past.getFullYear();
	    var past_m = (past.getMonth()+1) < 10? "0" + (past.getMonth()+1) : (past.getMonth()+1);
	    var past_d = past.getDate() < 10? "0" + past.getDate() : past.getDate();
	    
	    var todayString = today_y + "-" + today_m + "-" + today_d;
	    var pastString = past_y + "-" + past_m + "-" + past_d;
	
	    // set values to date-pickers BEFORE the date-pickers are initialized
	    // only the before-widgets, after will be set at "copyBeforeToAfter()"!
	    $("#date-to-before").val( todayString );
	    $("#date-from-before").val( pastString );
	    
	    // initialize the date-picker widgets AFTER the values were set
	    // only the before-widgets, after will be set at "copyBeforeToAfter()"!
		$('#date-from-container-before').datepicker();
		$('#date-to-container-before').datepicker();
	})();
	
	/**
	 * Initialize tooltips
	 */
	(function() {

		$("#timeline_link").tooltip({ 'placement': 'top', delay: { show: 500, hide: 100 } });
		$("#clusters_link").tooltip({ 'placement': 'top', delay: { show: 500, hide: 100 } });
        $("#comments_link").tooltip({ 'placement': 'top', delay: { show: 500, hide: 100 } });
        
        $("#submit-button").tooltip({ 'placement': 'right', delay: { show: 500, hide: 100 } });
        $("#submit-button-before").tooltip({ 'placement': 'right', delay: { show: 500, hide: 100 } });
        
        $("#search-text").tooltip({ 'placement': 'left', delay: { show: 500, hide: 100 } });
        $("#search-text-before").tooltip({ 'placement': 'left', delay: { show: 500, hide: 100 } });
	})();
	
	/**
	 * Initialize the rang inputs
	 */
	(function() {
		
		$("#similarity-before").rangeinput();
		$("#similarity").rangeinput();
	})();
	
	/**
	 * Initialize toggle button to topic search
	 */
	(function() {
		
		$("#toggle-topic-button-before").button('toggle');
	})();
	
	/**
	 * Initialize the more-link to display less on a click
	 */
	(function() {
		
		$("#more-less-before").click(function() {

			if($("#more-less-before").text() == "More...") {
				$("#more-less-before").text("Less...");
			}
			else {
				$("#more-less-before").text("More...");
			}
		});
		
		$("#more-less-after").click(function() {

			if($("#more-less-after").text() == "More...") {
				$("#more-less-after").text("Less...");
			}
			else {
				$("#more-less-after").text("More...");
			}
		});
	})();
	
	/**
	 * Set an hover effect for the tag cloud
	 */

	(function() {
		
		$(".topic-label").hover(
		  function () {
		    $(this).css("background-color", "#FFA526")
		  },
		  function () {
		    $(this).css("background-color", "#3399f3")
		  }
		);
		
	})();

	/**
	 * Removes all SVG charts (the time line, the cluster
	 * list, the pie chart).
	 */
	function removeAllSvgCharts() {
		
		d3.select("#timeline").select("svg").remove();
		d3.select("#clusters").select("svg").remove();
		d3.select("#comments").select("svg").remove();
	}

    function sortJson(json) {
        json.children.sort(function(a, b) {
            var aSim = a.similarity, bSim = b.similarity;
            if(aSim > bSim) return 1;
            if(aSim < bSim) return -1;
            return 0;
        });
        json.children.reverse();
    }
	
	/**
	 * Renders all SVG charts (the time line, the cluster
	 * list, the pie chart).
	 */
	function renderAllSvgCharts() {
		
		try {
			
			var before = new Date();
			
			$('#tech-trends-error').hide();
			$('#loading-timeline').show();
			$("#headline-searchword").text(readQuery());
			$(".timeline-date-text").html("Hover the char.<br>Click for links.<br>Zoom in.");
			
			var url = getSearchUrl();
			d3.json(url, function(json) {

				try {
					
					check( json )
                    sortJson(json);
		
					showResultInfo(json);
					renderTimeLine(json);
					renderClusters(json);
					renderComments(json);
				}
				catch( error ) {
					
					showErrorInfo( error );
					$('#loading-timeline').hide();
				}

				var after = new Date();
				appendToResultInfoBox("in " + (after - before) + " milliseconds");
				$('#loading-timeline').hide();
			});
		}
		catch( error ) {
			
			showErrorInfo( error );
			$('#loading-timeline').hide();
		}
	}
	
	/**
	 * Performs some simple checks on the given JSON and throws
	 * and exception with a good message if the JSON is not valid.
	 */
	function check( json ) {
		
		if( json ) {
			
			if( json.error ) {
				
				throw json.error;
			}
			
			if( json.children.length === 0 ) {
				
				throw "Sorry, we found nothing for your search.";
			}
		}
		else {
			
			throw "Sorry, no links retrieved. Something just exploded...";
		}
	}
	
	/**
	 * Add click-listener to submit-button.
	 */
	$("#submit-button").click(function(e) {
		
		removeAllSvgCharts();
		showSearchQueryInHeadline();
		renderAllSvgCharts();
	});
	
	/**
	 * Add enter-listener to search-field.
	 */
	$("#search-text").keydown(function(event) {
	
		// enter has keyCode 13
		if (event.keyCode == 13) {
		
			removeAllSvgCharts();
			showSearchQueryInHeadline();
			renderAllSvgCharts();
		}
	});
	
	/**
	 * Add click-listener to submit-button on welcome-page.
	 */
	$("#submit-button-before").click(function(e) {

		removeAllSvgCharts();
		hideBeforeScreenAndShowAfterScreen();
		copyBeforeToAfter();
		showSearchQueryInHeadline();
		renderAllSvgCharts();
	});
	
	/**
	 * Add enter-listener to search-field on welcome-page.
	 */
	$("#search-text-before").keydown(function(event) {
	
		// enter has keyCode 13
		if (event.keyCode == 13) {
		
			removeAllSvgCharts();	
			hideBeforeScreenAndShowAfterScreen();	
			copyBeforeToAfter();
			showSearchQueryInHeadline();
			renderAllSvgCharts();
		}
	});
	
	/**
	 * Add placeholder-change to togglebuttons.
	 */
	$("#toggle-topic-button-before").click(function(e) {
		$("#search-text-before").attr("placeholder", "Java");
	});
	$("#toggle-id-button-before").click(function(e) {
		$("#search-text-before").attr("placeholder", "77");
	});
	$("#toggle-link-button-before").click(function(e) {
		$("#search-text-before").attr("placeholder", "http://en.wikipedia.org/wiki/Mtgox#History");
	});
	$("#toggle-topic-button-after").click(function(e) {
		$("#search-text").attr("placeholder", "Java");
	});
	$("#toggle-id-button-after").click(function(e) {
		$("#search-text").attr("placeholder", "77");
	});
	$("#toggle-link-button-after").click(function(e) {
		$("#search-text").attr("placeholder", "http://en.wikipedia.org/wiki/Mtgox#History");
	});

	/**
	 * The search-box (including the date-pickers and similarity-slider) on the 
	 * before-search-screen (welcome-screen) is a different box then the one shown
	 * on the common page after a search. So we have to copy the given values
	 * from this box to the normal one.
	 */
	function copyBeforeToAfter() {
		
		$("#search-text").val( $("#search-text-before").val() );
		$("#date-from").val( $("#date-from-before").val() );
		$("#date-to").val( $("#date-to-before").val() );
		$("#similarity").data("rangeinput").setValue( $("#similarity-before").val() );
		
		copyBeforeToggleToAfterToggle();

		// re-initialize the date pickers
		$('#date-from-container-after').datepicker();
		$('#date-to-container-after').datepicker();
		$('#date-from-container-before').datepicker();
		$('#date-to-container-before').datepicker();
	}
	
	function copyBeforeToggleToAfterToggle() {

		if ( $('#toggle-topic-button-before').hasClass("active") ) {

			$('#toggle-topic-button-after').button('toggle');
		} 
		else if ( $('#toggle-link-button-before').hasClass("active") ) {

			$('#toggle-link-button-after').button('toggle');
		} 
		else {

			$('#toggle-id-button-after').button('toggle');
		}
	}
	
	/**
	 * Shows the search-query (the text in the search-input-field)
	 * in the headline (like "Searching for 'XXX'").
	 */
	function showSearchQueryInHeadline() {
		
		$("#headline-searchword").html( $("#search-text").text() );
	}
	
	/**
	 * Hides the before-search screen (the single box) and shows
	 * the after-search screen (the tabs with charts and all that
	 * stuff).
	 */
	function hideBeforeScreenAndShowAfterScreen() {
		
		$("#after-search").css( "display", "block" );
		$("#before-search").css( "display", "none" );
	}
	
	/**
	 * This function searches a specified keyword. It removes the old
	 * SVG-charts, puts the keyword in the search-field and calls
	 * renderTimeLine() and renderClusters() to perform the actual
	 * search.
	 * 
	 * This function is e.g. used by the tag-cloud when you click on a
	 * tag.
	 * 
	 * @param keyword to search
	 */
	function searchTopic( keyword ) {
		
		removeAllSvgCharts();
		$("#search-text").val( keyword );
		$('#toggle-topic-button-after').button('toggle');
		renderAllSvgCharts();
	}
	
	/**
	 * This function searches a specified id. It removes the old
	 * SVG-charts, puts the id in the search-field and calls
	 * renderTimeLine() and renderClusters() to perform the actual
	 * search.
	 * 
	 * @param id to search
	 */
	function searchId( id ) {
		
		removeAllSvgCharts();
		$("#search-text").val( id );
		$('#toggle-id-button-after').button('toggle');
		renderAllSvgCharts();
	}
	
	/**
	 * Append the search function to each tag in the tag-cloud
	 * on the right side (each tag is a topic-label).
	 */
	$('.topic-label').click(function(e) {

		var target = e.toElement || e.target;
		
		var text = $(target).text();
		
		searchTopic( text );
	});
	
	/**
	 * This function places some meta-information in the "result" box
	 * of the info page, e.g. how many articles we found.
	 * 
	 * @param url to get the info for
	 */
	function showResultInfo(json) {
		
		$('#result-info').empty();

		appendToResultInfoBox("Results for " + readQuery());
		appendToResultInfoBox(json.children.length + " articles found");
		appendToResultInfoBox("from " + readFrom() + " to " + readTo());
		appendToResultInfoBox("with " + readSimilarity()*100 + "% minimal similarity")
	}

	/**
	 * Show the passed message in an error box on top of the 
	 * page under the search-word title.
	 */
	function showErrorInfo(message) {

		$('#tech-trends-error').text( message );
		$('#tech-trends-error').show();
	}
	
	/**
	 * This function appends a text to the result info box of the index
	 * page (e.g. a message how many article were found).
	 * 
	 * @param message to append to the result info box
	 */
	function appendToResultInfoBox(message) {
		
		$('<br>').appendTo('#result-info');
		
		$('<span>', {
		    text: message
		}).appendTo('#result-info');
	}
	
	/**
	 * @returns value of the search field
	 */
	function readQuery() {

		return $('input[id=search-text]').val();
	}
	
	function setQuery( value ) {

		$('input[id=search-text]').val(value);
	}
	
	/**
	 * @returns value of the from-date picker
	 */
	function readFrom() {

		return $('input[id=date-from]').val();
	}
	
	function setFrom( value ) {

		$('input[id=date-from]').val(value);
	}
	
	/**
	 * @returns value of the to-date picker
	 */
	function readTo() {

		return $('input[id=date-to]').val();
	}
	
	function setTo( value ) {

		$('input[id=date-to]').val(value);
	}
	
	/**
	 * @returns value of the similarity range as an double between 0 and 1
	 */
	function readSimilarity() {
	
		return $('input[id=similarity]').val()/100;
	}
	
	function setSimilarity( value ) {

		$('input[id=similarity]').val(value * 100);
	}
	
	/**
	 * This function generates the search URL to get the JSON-result from the 
	 * server. It checks whether an ID is searched or a query. It should be 
	 * used in every function to get the search results!
	 */
	function getSearchUrl() {
		
		var url = "";
		
		if( $('#toggle-topic-button-after').hasClass("active") ) {
			
			url = "/search?query=" + readQuery() + "&min=" + readSimilarity() + "&from=" + readFrom() + "&to=" + readTo();
		}
		else if( $('#toggle-link-button-after').hasClass("active") ) {
			
			url = "/example?link=" + readQuery() + "&min=" + readSimilarity() + "&from=" + readFrom() + "&to=" + readTo();
		}
		else {
			
			url = "/similar?id=" + readQuery() + "&min=" + readSimilarity() + "&from=" + readFrom() + "&to=" + readTo();
		}
		
		location.hash = "#" + url;

		return url;
	}
	
	/**
	 * If we have a location hash value and no query, we take the location
	 * hash and parse it to fill the controlls and start a search.
	 */
	if( readQuery() === "" && location.hash !== "" ) {

		// Note: The order of the instructions matters!
		
		copyBeforeToAfter();
		
		parseLocationHash();
		
		removeAllSvgCharts();
		hideBeforeScreenAndShowAfterScreen();
		showSearchQueryInHeadline();
		renderAllSvgCharts();
	}
	
	/**
	 * Parses the location hash value of the url:
	 * 
	 * http://127.0.0.1:5000/#/search?query=bitcoin&min=0.18&from=2013-04-09&to=2013-06-09
	 * 
	 * This value is the string after "#". We rewrite this string on a search.
	 * 
	 */
	function parseLocationHash() {
		
		var loc = location.hash;
		
		var params = loc.substr(loc.indexOf("?") + 1).split('&');
		
		i = 0;
		for( i = 0; i < params.length; i++ ) {
			
			var param = params[i];
			var key = param.split('=')[0];
			var value = param.split('=')[1];

			if(key === "query" && value !== "") {
		
				setQuery( value );
				$('#toggle-topic-button-after').button('toggle');
			}
			
			if(key === "link" && value !== "") {
			
				setQuery( value );
				$('#toggle-link-button-after').button('toggle');
			}
			
			if(key === "id" && value !== "") {

				setQuery( value );
				$('#toggle-id-button-after').button('toggle');
			}
			
			if(key === "min" && value !== "") {
				
				setSimilarity( value );
			}
			
			if(key === "from" && value !== "") {
				
				setFrom( value );
			}
			
			if(key === "to" && value !== "") {

				setTo( value );
			}
		}
	}

	/**
	 * Make the initial popover.
	 */
	(function() {
		

		makePopover();
		
	})();
	
	function makePopover() {
		
		$('#headline-searchword').popover({
			
			position: 'absolute',
			placement: 'right',
			content: "",
			html: 'true',
			trigger: 'click',
			title : 'Links'+
            '<button type="button" id="close" class="close" onclick="$(&quot;#headline-searchword&quot;).popover(&quot;hide&quot;);">&times;</button>',
		});

		$("body").click(function( event ) {

			var target = event.toElement || event.target;
	
			if( 
					target.id !== "myPath" && 
					target.id !== "myPath2" && 
					target.id !== "triangle" && 
					target.className !== "popover-title" && 
					target.className !== "popover-content" &&
					target.className !== "popover-link"&&
					target.className !== "popover fade right in") {
			
				$('#headline-searchword').popover('hide');
			}
		});
	}
	
	/**
	 * @param content string to set in the popover
	 */
	function setPopoverContent( content ) {
		
		var popover = $('#headline-searchword').attr( 'data-content', content ).data('popover');
		popover.setContent();
	}
	
	/**
	 * @param title string to set in the popover
	 */
	function setPopoverTitle( title ) {
		
		// the close button in the upper right corner!
		var close = '<button type="button" id="close" class="close" onclick="$(&quot;#headline-searchword&quot;).popover(&quot;hide&quot;);">&times;</button>';
		
		var popover = $('#headline-searchword').attr( 'data-original-title', title + close ).data('popover');
		
		popover.setContent();
	}
	
	/**
	 * @param x (= left)
	 * @param y (= top)
	 */
	function setPopoverPosition( x, y ) {
		
		$('.popover').css({
			
			"top": y + "px",
		    "left": x  + "px"
		    
		});
	}
	
	techtrends.getSourceLink = getSourceLink = function( source ) {
		
		if($.isNumeric( source )) {
			
			return "<a class='popover-link' target='_blank' href='https://news.ycombinator.com/item?id=" + source + "'>Open discussion</a>";
		}
	
		return "<a class='popover-link' target='_blank' href='http://www.reddit.com/r/programming/comments/" + source + "'>Open discussion</a>";
	}
	
	/**
	 * Shared variables for the pie chart
	 */
	var comments_height = 400;
	var comments_width = 760;
	var comments_radius = Math.min(comments_width, comments_height) / 2;

	/**
	 * Shared variables for the time line
	 */
	var timeline_x;
	var timeline_margin;
	var timeline_data = [];
	
	/**
	 * Shows the popover for the pie-chart.
	 * 
	 * @param data to show
	 */
	function showCommentsPopover( data ) {

		contentString  = "<a class='popover-link' target='_blank' href='" + data.data.url + "'>" + data.data.title + "</a>";
		contentString += "<hr/>";
		contentString += "Popularity: " + data.data.popularity;
		contentString += "<br>" + getSourceLink( data.data.source );
		contentString += "<hr/>";
		contentString += "<b>From</b>: " + getShortDateString(new Date( data.data.first ));
		contentString += "<br><b>To</b>: " + getShortDateString(new Date( data.data.last ));
		contentString += "<hr/>";
		contentString += "<a id='searchFor' href='/#/search?id=" + data.data.id + "'>Re-search ID " + data.data.id + "</a>";

		setPopoverVisible();
		setPopoverContent( contentString );
		setPopoverTitle( "Link" );
		
		$('#searchFor').click(function(e) {

			searchId( data.data.id );
		});
		
		var mx = parseInt( d3.mouse(this)[0] );
		var my = parseInt( d3.mouse(this)[1] );

		var offsetL = parseInt( $('#comments').offset().left );
		var offsetT = parseInt( $('#comments').offset().top );
			
		var middleTop = offsetT + comments_height/2;
		var middleLeft = offsetL + comments_width/2;
			
    	var popWidth = parseInt( $('.popover').css("width").replace("px","") );
    	var popHeight = parseInt( $('.popover').css("height").replace("px","") );

    	var angle = Math.tan( my / comments_radius );

    	var x = Math.round(comments_radius * Math.cos(angle));
    	var y = Math.round(comments_radius * Math.sin(angle));

    	if( (mx <= 0 && my > 0) || (mx <= 0 && my <= 0) ) {

    		setPopoverPosition( (middleLeft - x - popWidth), (middleTop + y - popHeight/2) );
		}
		else if( (mx > 0 && my > 0) || (mx > 0 && my <= 0) ) {

			 setPopoverPosition( (middleLeft + x), (middleTop + y - popHeight/2) );
		}
	}
	
	/**
	 * Shows the popover for the timline.
	 * 
	 * @param data to visualize
	 * @param event
	 */
	function showTimelinePopover( data ) {

		data = timeline_data;
		
		// retrieves the links to the given date and displays them in a popover
	    var date = timeline_x.invert(mouse_x - $("#timeline").position().left - timeline_margin.left);
	
	    var i = 0;
	    while (data[i+1].date < date){
	    	
	    	i++;
	    }
	    
		var links = linksForDate(timeline_data, data[i].date, 14);
		links = removeDuplicates(links);

		var contentString = "";
		
		for(var i = 0; i < links.length; i++) {

			contentString += "<a class='popover-link' target='_blank' href='" + links[i].link + "'>" + links[i].title + "</a></p>"
		}

		setPopoverContent( contentString );
		setPopoverTitle( getShortDateString(date) );
		setPopoverVisible();
		setPopoverPosition( mouse_x + 20, mouse_y);	// 20px offset to the right
	}
	
	function getShortDateString(date) {
		var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		var dateString = date.getDate()+ " " + months[date.getMonth()] + " " + date.getFullYear();
		return dateString;
	}

	/**
	 * The global mouse position
	 */
	var mouse_x;
	var mouse_y;

	(function() {
	
		window.onmousemove = handleMouseMove;
		
		function handleMouseMove(event) {
		    	
		    event = event || window.event; // IE-ism
	
		    mouse_x = event.clientX;
		    mouse_y = event.clientY;
		}
	})();
	
	var drag = false;
	
	/**
	 * Shows the popover
	 */
	function setPopoverVisible(){
		
		$('#headline-searchword').popover('show');

		$('.popover').drags();
	}

	/**
	 * Copy & Paste - http://css-tricks.com/snippets/jquery/draggable-without-jquery-ui/
	 */
	(function($) {
	    $.fn.drags = function(opt) {

	        opt = $.extend({handle:"",cursor:"move"}, opt);

	        if(opt.handle === "") {
	            var $el = this;
	        } else {
	            var $el = this.find(opt.handle);
	        }

	        return $('.popover-title').css('cursor', opt.cursor).on("mousedown", function(e) {
	            if(opt.handle === "") {
	                var $drag = $('.popover').addClass('draggable');
	            } else {
	                var $drag = $('.popover').addClass('active-handle').parent().addClass('draggable');
	            }
	            var z_idx = $drag.css('z-index'),
	                drg_h = $drag.outerHeight(),
	                drg_w = $drag.outerWidth(),
	                pos_y = $drag.offset().top + drg_h - e.pageY,
	                pos_x = $drag.offset().left + drg_w - e.pageX;
	            $drag.css('z-index', 1000).parents().on("mousemove", function(e) {
	                $('.draggable').offset({
	                    top:e.pageY + pos_y - drg_h,
	                    left:e.pageX + pos_x - drg_w
	                }).on("mouseup", function() {
	                    $(this).removeClass('draggable').css('z-index', z_idx);
	                });
	            });
	            e.preventDefault(); // disable selection
	        }).on("mouseup", function() {
	            if(opt.handle === "") {
	            	$('.popover').removeClass('draggable');
	            } else {
	            	$('.popover').removeClass('active-handle').parent().removeClass('draggable');
	            }
	        });

	    }
	})(jQuery);

	/**
	 * Render the pie-charts showing each found article as big as
	 * its comments and votes are.
	 */
	function renderComments(json) {

		var color = d3.scale.ordinal().range(["#000000", "#050F1A", "#0A1F33", "#0F2E4C", "#143D66", "#1A4C80", "#D6EBFF",
											  "#1F5C99", "#246BB2", "#297ACC", "#2E8AE6", "#3399FF", "#47A3FF", "#EBF5FF",
											  "#5CADFF", "#70B8FF", "#85C2FF", "#99CCFF", "#ADD6FF", "#C2E0FF", "#FFFFFF"]);
		
		var arc = d3.svg.arc().outerRadius(comments_radius - 10).innerRadius(0);
	
		var pie = d3.layout.pie().sort(null).value(function(d) {
			
			return d.popularity;
		});
	
		var svg = d3.select(".comments-class")
						.append("svg")
						.attr("width", comments_width)
						.attr("height", comments_height)
						.append("g")
						.attr("transform", "translate(" + comments_width / 2 + "," + comments_height / 2 + ")");
		
		var data = [];
		
		json.children.forEach(function(d) {
			
			data.push( { title: d.title, id: d.id, popularity: d.popularity, url: d.url, source: d.source, first: d.first, last: d.last } );
		});

		var pieChart = svg
						.selectAll(".arc")
						.data(pie(data))
						.enter()
						.append("g")
						.attr("class", "arc")
						.attr("class", "clickable arc");
		
		pieChart
			.append("path")
			.attr("d", arc)
			.attr("id", "triangle")
			.attr("class", "clickable arc")
			.style("fill", function(d) { return color(d.data.title); });
		
		pieChart
			.attr("d", arc)
			.attr("id", "triangle")
			.attr("class", "clickable arc")
			.on('click', showCommentsPopover)
			.style("fill", function(d) { return color(d.data.title); });
	}

	function removeDuplicates(links) {
		
		if (links.length == 1) {	
			return links;
		}
		
		var uniqueLinks = [];
		
		for(var i = 0; i < links.length; i++){

			var isAlreadyInUniqueLinks = false;
			for (var k = 0; k < uniqueLinks.length; k++) {
				if (uniqueLinks[k].title == links[i].title) {
					isAlreadyInUniqueLinks = true;
				}
			}
			if (!isAlreadyInUniqueLinks) {
				uniqueLinks.push(links[i]);
			}
		}
		
		return uniqueLinks;
	}

	function linksForDate(data, date, timeFrame) {
		
		var minDate = new Date(date);
		var maxDate = new Date(date);
		minDate.setDate(minDate.getDate()-timeFrame/2);
		maxDate.setDate(maxDate.getDate()+timeFrame/2);
		
		result = [];
		
		for(var k=0; k<data.length; k++) {
			var map = data[k];
			var votes = map.votes;
			var date = new Date(map.date);
			var links = map.links;
			
			if (date > minDate && date < maxDate) {
				
				for(var i=0; i<links.length; i++) {
					var tuple = links[i];
					result.push(tuple);
				}
			}
		}
		return result;
	}

	function displayCurrentDate( data ) {
		
		cursor(data);
		
		var date = timeline_x.invert(mouse_x - $("#timeline").position().left - timeline_margin.left);
		
		if (date == null) {
			return;
		}
		
		var popularity = "";
		var articles = "";
		
		date.setHours(0, 0, 0, 0);

		var i = 0;
		for(i = 0; i < data.length; i += 1) {

			if(data[i].date - date === 0) {

				popularity = data[i].popularity;
				articles = linksForDate(timeline_data, data[i].date, 14).length - 1;
				break;
			}
		}
		
		popularity = "" + popularity;
		popularity = "Popularity: " + popularity.substring(0, 5);
		
		articles = "Articles: " + articles;
		
		var text = getShortDateString(date) + "<br>" + popularity + "<br>" + articles;

		$(".timeline-date-text").html(text);
	}

	var lookAhead = 2 * (24 * 60 * 60 * 1000);
	
	function cursor(d) {

		d3.select("#myPath2").remove();

		var date = timeline_x.invert(mouse_x - $("#timeline").position().left - timeline_margin.left);

		date.setHours(0, 0, 0, 0);

		var before = new Date(date.getTime() + lookAhead);

		var i = 0;

		var data = []

		for (i = 0; i < d.length; i++) {

			var obj = d[i];
			
			obj.date.setHours(0, 0, 0, 0);

			if (date <= obj.date && before > obj.date) {

				data.push(obj);
			}
		}

		focus
			.append("path")
			.datum(data)
			.attr("id", "myPath2")
			.on('click', showTimelinePopover)
			.attr("clip-path", "url(#clip)")
			.attr("d", area);
	}
	
	var focus;
	var area;

	/**
	 * Renders the time line showing when each article occurs.
	 */
	function renderTimeLine(json) {

		var margin = {top: 10, right: 10, bottom: 100, left: 40},
		    margin2 = {top: 430, right: 10, bottom: 20, left: 40},
		    width = 760 - margin.left - margin.right,
		    height = 500 - margin.top - margin.bottom,
		    height2 = 500 - margin2.top - margin2.bottom;

		var x = d3.time.scale().range([0, width]),
		    x2 = d3.time.scale().range([0, width]),
		    //y = d3.scale.linear().range([height, 0]),
		    y = d3.scale.pow().exponent(.2).range([height, 0]),
		    //y2 = d3.scale.linear().range([height2, 0]);
		    y2 = d3.scale.pow().exponent(.4).range([height2, 0]);

		timeline_x = x;
		timeline_y = y;
		timeline_margin = margin;
		
		var xAxis = d3.svg.axis().scale(x).orient("bottom"),
		    xAxis2 = d3.svg.axis().scale(x2).orient("bottom"),
		    yAxis = d3.svg.axis().scale(y).orient("left");

		var brush = d3.svg.brush()
		    .x(x2)
		    .on("brush", brushed);

		area = d3.svg.area()
		    .interpolate("step-after")
		    .x(function(d) { return x(d.date); })
		    .y0(height)
		    .y1(function(d) { return y(d.popularity); });

		var area2 = d3.svg.area()
		    .interpolate("step-after")
		    .x(function(d) { return x2(d.date); })
		    .y0(height2)
		    .y1(function(d) { return y2(d.popularity); });

		var svg = d3.selectAll(".timeline-class").append("svg")
		    .attr("width", width + margin.left + margin.right)
		    .attr("height", height + margin.top + margin.bottom);

	 	var hoverLineGroup = svg
  		.append("svg:g")
  			.attr("class", "line")
		.append("rect")
			.attr("id", "hover-cursor")
			.attr("width", 1)
			.attr("height", height + 80)
			.attr("x", 0)
			.attr("y", 10)
			.style("fill", "black");
		
		svg.append("defs").append("clipPath")
		    .attr("id", "clip")
		    .append("rect")
		    .attr("width", width)
		    .attr("height", height);
		
		svg.append("text")
	    .attr("class", "y label")
	    .attr("text-anchor", "end")
	    .attr("y", 6)
	    .attr("dy", "3.75em")
	    .attr("transform", "rotate(-90)")
	    .text("Popularity");

		focus = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

		var context = svg.append("g").attr("transform", "translate(" + margin2.left + "," + margin2.top + ")");

		json.children.forEach(function(d) {

			d.first = new Date(d.first * 1000);
			d.last = new Date(d.last * 1000);

			d.first.setHours(0, 0, 0, 0);
			d.last.setHours(0, 0, 0, 0);
			  
			timeline_data.push( d );
		});
		
		  var accumulatedData = [];
		  var curDate = d3.min(timeline_data.map(function(d) { return d.first; }));
		  var minDate = d3.min(timeline_data.map(function(d) { return d.first; }));
		  var maxDate = d3.max(timeline_data.map(function(d) { return d.last; }));
		  
		  curDate.setHours(0, 0, 0, 0);
		  
		  while(curDate < maxDate) {
			  
			  var accumulatedVotes = 0;
			  var articles = 0;
			  var links = [];
			  
			  timeline_data.forEach(function(d) {

				  if (isBetween(curDate, d.first, d.last)) {

					  accumulatedVotes += d.popularity;
					  articles += 1;
					  
					  links.push( {"link": d.url, "title":d.title} );
				  }
			  });
			  
			  accumulatedData.push({"popularity": accumulatedVotes, "articles": articles, "votes": accumulatedVotes, "date": new Date(curDate), "links": links});

			  curDate.setDate(curDate.getDate() + 1);
		  }

		  timeline_data = accumulatedData;
		  
		  // average over the complete dataset
		  var average = 0;
		  
		  // only the entries with articles
		  // average is calculated using the same loop, performance, u know.
		  var dataWithoutZeroDays = [];
		  timeline_data.forEach(function(d) {
		  	if (d.popularity > 0.001) {
		  		dataWithoutZeroDays.push(d);
		  	}
		  	average += d.popularity;
		  });
		  
		  average /= timeline_data.length;
		  calculateTrend(average, timeline_data);
		  

		  y.domain([0, 500]);

		  var readFromTemp = asDateObject(readFrom());
		  var readToTemp = asDateObject(readTo());
		  
		  x.domain([readFromTemp, readToTemp]);
		  x2.domain(x.domain());
		  y2.domain(y.domain());
		  
		  focus.append("path")
	          .attr("id", "pathWithoutZeros")
		      .datum(dataWithoutZeroDays)
		      .attr("clip-path", "url(#clip)")
		      .attr("d", area);

		  focus.append("path")
		      .datum(timeline_data)
	          .attr("id", "myPath")
	          .on('click', showTimelinePopover)
			  .on('mousemove', displayCurrentDate)               
		      .attr("clip-path", "url(#clip)")
		      .attr("d", area);

		  focus.append("title");

		  focus.append("g")
		      .attr("class", "x axis")
		      .attr("transform", "translate(0," + height + ")")
		      .call(xAxis);

		  focus.append("g")
		      .attr("class", "y axis")
		      .call(yAxis);

		  context.append("path")
		      .datum(timeline_data)
		      .attr("d", area2);
		      
		  context.append("g")
		      .attr("class", "x axis")
		      .attr("transform", "translate(0," + height2 + ")")
		      .call(xAxis2);

		  context.append("g")
		      .attr("class", "x brush")
		      .call(brush)
		    .selectAll("rect")
		      .attr("y", -6)
		      .attr("height", height2 + 7);
		      
		      
		  	svg.on('mousemove', function(d) {

		  		handleMouseOverGraph( parseInt( d3.mouse(this)[0] ) );
		 	});

			hoverLineGroup.classed("hide", true);
			
			function handleMouseOutGraph() {	

				hoverLineGroup.classed("hide", true);
			}
			
			function handleMouseOverGraph(mx) {	

				hoverLineGroup.classed("hide", false);
				hoverLineGroup.attr("x", mx);
			}

			function brushed() {
				x.domain(brush.empty() ? x2.domain() : brush.extent());
				//focus.select("path").attr("d", area);
				focus.selectAll("path").attr("d", area);
				focus.select(".x.axis").call(xAxis);
			}
			
	}
	
	/**
	 * Returns -1 for negative trend, 1 for positive trend and 0 for no trend
	 */
	function calculateTrend(average, timeline_data) {
		var size = timeline_data.length;
		
		// We take a quarter of the datasets range to calculate the trend indicator
		var range = size/3.0;
		
		// For the epsilon comparison, we need an epsilon..
		var deltaPos = 0.2 * average;
		var deltaNeg = 0.2 * average;
		
		// Now, the average popularity for the chosen range is calculated
		var averagePopOfTheLastXEntries = 0.0;
		for (var i = 0; i < range; i++) {
			averagePopOfTheLastXEntries += timeline_data[size - 1 - i].popularity;
		}
		averagePopOfTheLastXEntries /= range;

		// At last, we have to append the correct image and return the result, if needed later
		var result = 0;
		var currentArrowRotationInDegrees = $(".timeline-trend-graphic").css('borderSpacing');
		$(".timeline-trend-graphic").remove();
		$(".timeline-trend-text").remove();
		
		// calculate the percentage our trend indicator is above/under the given average to
		// have different angles for the trend arrow
		var percentage = ((averagePopOfTheLastXEntries) - average) / average;
		
		// Translate the percentage into percentspace and because we're not using 360 degrees range,
		// we can devide by to, to have nicer resulting angles. Also, we limit the angle from -45
		// to 45 degrees.
		percentage *= 100 / 2;
		var targetDegree = 0;
		if (percentage < 0) {
			targetDegree = Math.max(percentage, -45);
		} else if (percentage > 0) {
			targetDegree = Math.min(percentage, 45);
		}
		
		if (averagePopOfTheLastXEntries > average + deltaPos) {
			
		$("#trend-image-div").append("<img src='static/img/arrow_green.png' class='timeline-trend-graphic rot-arrow'></img>");
			$(".timeline-trend-graphic").tooltip({placement:"left",title:"The topic shows an aspiring trend."});
			result = 1;
			
		} else if (averagePopOfTheLastXEntries < average - deltaNeg) {
			
		$("#trend-image-div").append("<img src='static/img/arrow_red.png' class='timeline-trend-graphic rot-arrow'></img>");
			$(".timeline-trend-graphic").tooltip({placement:"left",title:"The topic shows a declining trend."});
			result = -1;
			
		} else {
			
			$("#trend-image-div").append("<img src='static/img/arrow_yellow.png' class='timeline-trend-graphic rot-arrow'></img>");
			$(".timeline-trend-graphic").tooltip({placement:"left",title:"The topic doesn't show a clear trend at the moment."});
			
		}
		
			
		$(".timeline-trend-graphic").css({borderSpacing: currentArrowRotationInDegrees});
		$(".timeline-trend-graphic").animate({borderSpacing: -targetDegree}, 
			{
				step: techtrends.arrowRotateStepFunction
			}, 2000);
	
		return result;
	}
	
	techtrends.arrowRotateStepFunction = function(now,fx) {
		$(this).css('-webkit-transform','rotate('+now+'deg)');
		$(this).css('-moz-transform','rotate('+now+'deg)'); 
		$(this).css('-ms-transform','rotate('+now+'deg)');
		$(this).css('-o-transform','rotate('+now+'deg)');
		$(this).css('transform','rotate('+now+'deg)'); 
	}
	
	/**
	 * Note: This function is also used in techtopics.js!
	 */
	techtrends.isBetween = isBetween = function(test, start, end) {
		return (test <= end && test >= start);
	}
	
	/**
	 * Returns a js dateobject from a given string.
	 * 
	 * Note: This function is also used in techtopics.js!
	 */
	techtrends.asDateObject = asDateObject = function(string) {
		var splitted = string.split("-");
		
		// Because the datestring contains a month between 0 and 11 instead of
		// 1 and 12, wo have to correct this
		if (splitted[1].substring(0, 1) == "0") {
			var temp = (parseInt(splitted[1].substring(1,2))-1)
			splitted[1] = "0" + temp;
		} else {
			var temp = (parseInt(splitted[1].substring(0,2))-1)
			splitted[1] = "" + temp;
		}
		var dateObject = new Date(splitted[0], splitted[1], splitted[2], 0,0,0,0);
		return dateObject;
	}
	
	// *******************************************************************
	// *******************************************************************
	// *******************************************************************
	// *******************************************************************
	// CLUSTER RENDERING *************************************************
	// *******************************************************************
	// *******************************************************************
	// *******************************************************************
	// *******************************************************************
	
	/**
	 * @param value to check if it's in the list
	 * @param list that might contain the value
	 * @returns {Boolean} true if the value is in the list
	 */
	function isIn(value, list) {
		
		var found = false;
	
		for(var i = 0; i < list.length; i++) {
			
			if (list[i].id == value.id) {
				
				found = true;
				break;
			}
	    }
			
		return found;
	}
	
	/**
	 * @param node to look for similar documents
	 * @param nodes that are already added to the tree
	 */
	function searchSimilar(node, nodes, update) {
	
		if (node.children == null && node._children == null) {
	
			d3.json("/similar?min=" + readSimilarity() + "&id=" + node.id,
	
			function(jsonData) {
	
				// we only add new nodes to the tree
				new_nodes = []
	
				var i = 0;
	
				for (i = 0; i < jsonData.children.length; i++) {
	
					var child = jsonData.children[i]
	
					if (!isIn(child, nodes))
						new_nodes.push(child)
				}
	
				node.children = new_nodes;
				node._children = null;
				
				update(node);
			});
		}
	}
	
	/**
	 * @param node to toggle (= collabse)
	 */
	function toggleChildrenOnClick(node) {
		
		if (node.children) {
			
			node._children = node.children;
			node.children = null;
		} 
		else {
			
			node.children = node._children;
			node._children = null;
		}
	}
	
	/**
	 * @param node to append the link box to
	 * @param x coordinate of the link box
	 * @param y coordinate of the link box
	 * @param width of the link box
	 * @param height of the link obx
	 */
	function appendLinkBox(node, x, y, width, height) {
		
		node
			.append("a")
				.attr("xlink:href", function(d) { return d.url; })
				.attr("target", function(d) { return d.url; })
			.append("rect")
		      	.attr("class", "clickable cluster-bar cluster-bar-link")
		      	.attr("x", x)
		      	.attr("y",   y)
		      	.attr("width", width)
		      	.attr("height", height);
	}
	
	function renderClusters(json) {
	
		var w = 760, h = 800, i = 0, barHeight = 30, barWidth = w, duration = 400, root;
	
		var tree = d3.layout.tree().size([h, 100]);
	
		var diagonal = d3.svg.diagonal().projection(function(d) { return [d.y, d.x]; });
		
		var vis = d3.select("#clusters")
			.append("svg:svg")
		    	.attr("width", w)
		    	.attr("height", h)
		    	.attr("id", "clustersvg")
		    .append("svg:g")
		    	.attr("id", "clusterg")
		    	.attr("transform", "translate(20,30)");

		json.x0 = 0;
		json.y0 = 0;
		update(root = json);

		function update(source) {
	
		  nodes = tree.nodes(root);
		  
		  // Compute the "layout".
		  nodes.forEach(function(n, i) {
			  
			  n.x = i * barHeight;
		  });
		  
		  // Update the nodes…
		  var node = vis.selectAll("g.node").data(nodes, function(d) { return d.id || (d.id = ++i); });
		  
		  var nodeEnter = node.enter().append("svg:g")
		      .attr("class", "node")
		      .attr("transform", function(d) { return "translate(" + source.y0 + "," + source.x0 + ")"; })
		      .style("opacity", 1e-6);
	
		  // Enter any new nodes at the parent's previous position.
		  nodeEnter
		  		.append("svg:rect")
		  			.attr("class", "cluster-bar")
		  			.attr("y", -barHeight / 2)
		  			.attr("height", barHeight)
		  			.attr("width", barWidth)
		  			.on("click", click);
		  
		  nodeEnter.append("svg:text")
		      .attr("dy", 3.5)
		      .attr("dx", 5.5)
		      .text(function(d) { return "[Open] " + d.title; });
		  
		  appendLinkBox(nodeEnter, 5, -6, 35, 12);
		  
		  // Transition nodes to their new position.
		  nodeEnter.transition()
		      .duration(duration)
		      .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; })
		      .style("opacity", 1);
		  
		  node.transition()
		      .duration(duration)
		      .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; })
		      .style("opacity", 1)
		    .select("rect")
		      .style("fill", color);
		  
		  // Transition exiting nodes to the parent's new position.
		  node.exit().transition()
		      .duration(duration)
		      .attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
		      .style("opacity", 1e-6)
		      .remove();
		  
		  // Update the links…
		  var link = vis.selectAll("path.link").data(tree.links(nodes), function(d) { return d.target.id; });
		  
		  // Enter any new links at the parent's previous position.
		  link.enter().insert("svg:path", "g")
		      .attr("class", "link")
		      .attr("d", function(d) {
		        var o = {x: source.x0, y: source.y0};
		        return diagonal({source: o, target: o});
		      })
		    .transition()
		      .duration(duration)
		      .attr("d", diagonal);
		  
		  // Transition links to their new position.
		  link.transition()
		      .duration(duration)
		      .attr("d", diagonal);
		  
		  // Transition exiting nodes to the parent's new position.
		  link.exit().transition()
		      .duration(duration)
		      .attr("d", function(d) {
		        var o = {x: source.x, y: source.y};
		        return diagonal({source: o, target: o});
		      })
		      .remove();
		  
		  // Stash the old positions for transition.
		  nodes.forEach(function(d) {
			  d.x0 = d.x;
			  d.y0 = d.y;
		  });
		  
		  // If nodes are expanded, the size of the svg should be changed, too.
		  var svg = $("#clustersvg");
		  var g = $("#clusterg");
		  
		  // each element is barHeight height											* barHeight
		  // childElementCount are all elements including the root node			 		- 1
		  // each element under the root counts double since it's the element itself
		  // and the gray line connecting the root and the element					    /2
		  // the root is not at the very top, it's one bar under it						+ barHeight * 2
		  
		  svg.attr("height", (g[0].childElementCount - 1)/2 * barHeight + barHeight * 2);
		}
		
		function click(node) {
		  
			toggleChildrenOnClick(node);
			update(node);
			
			searchSimilar(node, nodes, update);
		}
	
		function color(d) {
			
		  return d._children ? "#3182bd" : d.children ? "#c6dbef" : "#FFFFFF";
		}
	}
}

techtrends();
