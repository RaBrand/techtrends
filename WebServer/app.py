from flask import Flask, render_template, jsonify, request
from flask.ext.assets import Environment, Bundle
from datetime import datetime
from time import mktime
from config import Config
from nltk.stem.wordnet import WordNetLemmatizer
import Pyro4
import db
from math import log
from SimilarityServer import link_to_tokens
from collections import Counter
if not Config.DEBUG:
    from werkzeug.contrib.cache import MemcachedCache
    cache = MemcachedCache(['127.0.0.1:11211'], default_timeout=24 * 60 * 60)

app = Flask(__name__)

# Asset Management
# add all JavaScript libraries/files that should be
# combined to one single file.
# Note: Don't add d3.v3.min.js since it will result in
# a syntax error on minification, so it's left out and added
# separately in the base.html. I tested both, the normal
# and the min version of the current release. Both result
# in the same error.
assets = Environment(app)

# enables/disables the packaging
if Config.DEBUG:
    assets.debug = True

js = Bundle(
    'js/jquery.min.js',
    'js/jquery.tools.min.js',
    'js/bootstrap.min.js',
    'js/bootstrap-datepicker.js',
    'js/jquery.dataTables.min.js',
    'js/techtrends.js',
    filters='jsmin', output='gen/packed.js')

css = Bundle(
    'css/jquery.dataTables.css',
    'css/bootstrap.min.custom.css',
    'css/datepicker.css',
    'css/techtrends.css',
    'css/bootstrap-responsive.min.css',
    output='gen/packed.css')

assets.register('js_all', js)
assets.register('css_all', css)

service = Pyro4.Proxy(Pyro4.locateNS().lookup('gensim.server'))
lemmatizer = WordNetLemmatizer()


def datetimeformat(date_float, date_format='%d.%b %H:%M'):
    return datetime.fromtimestamp(date_float).strftime(date_format)
app.jinja_env.filters['datetimeformat'] = datetimeformat


def fetch_links_by_ids_with_sim(ids, sims):

    links = db.fetch_links_by_ids(ids)
    return [dict(link, **{'similarity': sims[str(link['id'])]}) for link in links]


def fetch_links_by_ids_and_date_with_sim(ids, start, end, sims):

    links = db.fetch_links_by_ids_and_date(ids, start, end)
    return [dict(link, **{'similarity': sims[str(link['id'])]}) for link in links]


def parse_date(date, ISO_8601='%Y-%m-%d'):

    try:
        date = datetime.strptime(date, ISO_8601).date()
        return mktime(date.timetuple())
    except ValueError:
        raise ValueError('Wrong format for date {date} (has to be like YYYY-MM-DD)'.format(date=date))


def get_main_words_for_topics(topics):

    # make a list of the main words. a main word
    # is a single word that stands for the topic,
    # something like it's title
    main_words = []

    for topic in topics:

        main_word = None

        for word in topic:
            if word[0] > main_word and word[1] not in main_words:
                main_word = word[1]

        main_words.append(main_word)

    return main_words


def get_sorted_topics(limit, words=20):

    lsi = service.debug_model().lsi
    topics = lsi.show_topics(limit, num_words=words, formatted=False)

    for topic in topics:
        topic.sort(key=lambda topic: topic[0], reverse=True)

    return topics


def error_json(error):

    return jsonify(error=str(error))


def param(name, default, cast=str):

    return cast(request.args.get(name, default))


def perform_query(query, start, end, score):
    '''
        Performs the given query (a dict {'tokens' : []}) against
        the similarity server, sorts the results and fetches the
        link for it.

        It's common for all 3 query methods - query, id and link.
    '''

    results = service.find_similar(query, score)
    ids = [result[0] for result in results]
    sims = dict([(result[0], result[1]) for result in results])
    links = fetch_links_by_ids_and_date_with_sim(ids, start, end, sims)

    return links


if not Config.DEBUG:
    @app.before_request
    def from_cache():

        response = cache.get(request.url.encode('utf-8'))
        if response:
            return response

    @app.after_request
    def to_cache(response):

        cache.add(request.url.encode('utf-8'), response)
        return response


@app.route('/example')
def links_by_example_and_timestamp():

    try:

        link = param("link", "")
        start = param("from", "2013-01-01")
        end = param("to", "2020-01-01")
        score = param("min", "0.7", float)
        top = param("top", "3", int)

        start = parse_date(start)
        end = parse_date(end)

        # parse the link to a list of words
        tokens = link_to_tokens(link)

        # get a sorted list of tupels (occurrence, word)
        mosts = Counter(tokens).most_common()

        # get only the first 'top' words as strings
        mosts = [str(most[0]) for most in mosts[0:top]]

        # build a query for the similarity server
        query = {'tokens': mosts}

        links = perform_query(query, start, end, score)

        return jsonify(id=0, title=link, children=links, first=start, last=end)

    except ValueError as e:
        return error_json(e)

    except Exception as e:
        return error_json(e)


@app.route('/search')
def links_by_query_and_timestamp():

    try:

        query = param("query", "")
        start = param("from", "2013-01-01")
        end = param("to", "2020-01-01")
        score = param("min", "0.7", float)
        pre = 'True' == param("pre", "True")

        start = parse_date(start)
        end = parse_date(end)

        tokens = query.lower().split(' ')

        # pre-process the search-query
        if pre:
            tokens = [lemmatizer.lemmatize(token) for token in tokens]

        links = perform_query({'tokens': tokens}, start, end, score)

        return jsonify(id=0, title=query, children=links, first=start, last=end)

    except ValueError as e:
        return error_json(e)


@app.route('/similar')
def links_by_id_and_timestamp():

    try:

        id = param("id", "0", int)
        start = param("from", "2013-01-01")
        end = param("to", "2020-01-01")
        score = param("min", "0.7", float)

        start = parse_date(start)
        end = parse_date(end)

        links = perform_query(str(id), start, end, score)

        return jsonify(id=0, title=str(id), children=links, first=start, last=end)

    except ValueError as e:
        return error_json(e)


@app.route('/list')
def links_by_time():

    try:

        limit = param("limit", "10", int)
        offset = param("offset", "0", int)
        start = param("from", "2013-01-01")
        end = param("to", "2020-01-01")

        startDate = parse_date(start)
        endDate = parse_date(end)

        links = db.fetch_links_by_date_with_votes(startDate, endDate, limit, offset)
        links = [dict(link) for link in links]

        return jsonify(children=links, first=startDate, last=endDate)

    except ValueError as e:
        return error_json(e)


@app.route('/datatable')
def datatable():

    try:

        sEcho = param("sEcho", "0", int)
        offset = param("iDisplayStart", "0", int)
        limit = param("iDisplayLength", "10", int)

        links = db.fetch_links_with_votes(limit, offset)
        links = [dict(link) for link in links]

        number = db.number_of_links()

        return jsonify(sEcho=sEcho+1, iTotalDisplayRecords=number, iTotalRecords=number, aaData=links)

    except ValueError as e:
        return error_json(e)


@app.route('/trending')
def topics():

    try:

        limit = param("limit", "10", int)
        words = param("words", "20", int)
        score = param("min", "0.4", float)

        raw_topics = get_sorted_topics(limit, words)

        main_words = get_main_words_for_topics(raw_topics)

        topics = []

        for word in main_words:

            query = {'tokens': [word]}

            results = service.find_similar(query, min_score=score)
            ids = [result[0] for result in results]
            sims = dict([(result[0], result[1]) for result in results])

            links = fetch_links_by_ids_with_sim(ids, sims)

            topics.append({ "title": word, "children": links })

        return jsonify(topics=topics)

    except ValueError as e:
        return error_json(e)


@app.route('/archive')
def archive():

    return render_template('archive.html')


@app.route('/topics')
def list_topics():

    limit = param("limit", "10", int)
    words = param("words", "20", int)

    topics = get_sorted_topics(limit, words)
    main_words = get_main_words_for_topics(topics)

    return render_template('topics.html', topics=topics, main=main_words)


@app.route('/api')
def api():

    return render_template('api.html')


@app.route('/contact')
def contact():

    return render_template('contact.html')


@app.route('/about')
def about():

    return render_template('about.html')


@app.route('/')
def client():

    topics = get_sorted_topics(25)
    main_words = get_main_words_for_topics(topics)

    main_words_with_size = []

    for topic, main_word in zip(topics, main_words):

        query = {'tokens': [tupel[1] for tupel in topic if tupel[0] > 0]}

        results = service.find_similar(query, 0.5)

        main_words_with_size.append({"word": main_word, "size": log(7 + len(results)) * 7})

    return render_template('index.html', main=main_words_with_size)
