CREATE TABLE IF NOT EXISTS links (
    id INTEGER PRIMARY KEY NOT NULL,
    url TEXT UNIQUE NOT NULL,
    html BLOB
);
CREATE INDEX IF NOT EXISTS url_index ON links(url);

CREATE TABLE IF NOT EXISTS hackernews (
    id TEXT PRIMARY KEY NOT NULL,
    first TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    last TIMESTAMP NOT NUll,
    title TEXT NOT NULL,
    votes INTEGER NOT NULL,
    comments INTEGER NOT NULL,
    url INTEGER NOT NULL,
    FOREIGN KEY(url) REFERENCES links(id)
);
CREATE INDEX IF NOT EXISTS hn_first ON hackernews(first);
CREATE INDEX IF NOT EXISTS hn_last ON hackernews(last);

CREATE TABLE IF NOT EXISTS reddit (
    id TEXT PRIMARY KEY NOT NULL,
    first TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    last TIMESTAMP NOT NUll,
    title TEXT NOT NULL,
    votes INTEGER NOT NULL,
    comments INTEGER NOT NULL,
    url INTEGER NOT NULL,
    FOREIGN KEY(url) REFERENCES links(id)
);
CREATE INDEX IF NOT EXISTS rdt_first ON reddit(first);
CREATE INDEX IF NOT EXISTS rdt_last ON reddit(last);

CREATE TABLE IF NOT EXISTS avg_var (
    type TEXT NOT NULL UNIQUE,
    votes_avg REAL NOT NULL,
    votes_var REAL NOT NULL,
    comments_avg REAL NOT NULL,
    comments_var REAL NOT NULL,
    duration_avg REAL NOT NULL,
    duration_var REAL NOT NULL
);

CREATE TABLE IF NOT EXISTS max_vals (
    type TEXT NOT NULL UNIQUE,
    max_votes REAL NOT NULL,
    max_comments REAL NOT NULL,
    max_duration REAL NOT NULL
);
