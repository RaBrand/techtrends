import re
from os import path
from datetime import date

"""
    Change the path to a new simserver in the config file
"""


def change_simserver_path():

    with open('config.py', 'r') as f:
        config = f.read()

        simserver_path = re.search(
            r"SIMILARITY_SERVER = '(.*)'", config
        ).group(1)
        today = date.today()
        new_simserver_name = 'techtrends_%s' % today.strftime('%d_%m_%Y')
        new_simserver_path = path.join(
            path.dirname(simserver_path), new_simserver_name
        )

        config = re.sub(
            r"SIMILARITY_SERVER = '(.*)'",
            r"SIMILARITY_SERVER = '%s'" % new_simserver_path,
            config
        )

    with open('config.py', 'w') as f:
        f.write(config)

if __name__ == '__main__':

    change_simserver_path()
