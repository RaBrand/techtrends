from config import Config
from preprocess import create_corpus
from simserver import SessionServer
import logging


logging.basicConfig(format=Config.LOG_FORMAT, level=Config.LOG_LEVEL)


def train_server():

    server = SessionServer(Config.SIMILARITY_SERVER)

    corpus = create_corpus()

    server.train(corpus)
    server.index(corpus)
