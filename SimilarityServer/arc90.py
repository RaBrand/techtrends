'''
based on:

                http://nirmalpatel.com/fcgi/hn.py

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''

import re

from bs4 import BeautifulSoup
from bs4 import Comment
from bs4 import Tag

# a list of IDs or CLASSes with a negative meaning
# Assumption: those tags will hold no content!
# Consequence: those tags will give minus points
NEGATIVE = re.compile(".*comment.*|.*meta.*|.*footer.*|.*footnote.*|.*foot.*|.*cloud.*|.*widget.*|.*head.*")

# a list of IDs or CLASSes with a positive meaning
# Assumption: those tags will hold content!
# Consequence: those tags will give plus points
POSITIVE = re.compile(".*post.*|.*hentry.*|.*entry.*|.*content.*|.*text.*|.*body.*|.*article.*")

BR = re.compile("<br */? *>[ \r\n]*<br */? *>")


def render_html(html):

    soup = BeautifulSoup(html)

    _delete_all_tags(soup, "link")
    _delete_all_tags(soup, "style")
    _delete_all_tags(soup, "script")
    _delete_all_tags(soup, "code")
    _delete_all_tags(soup, "form")
    _delete_all_tags(soup, "object")
    _delete_all_tags(soup, "iframe")
    _delete_all_tags(soup, "img")
    _delete_all_tags(soup, "embed")
    _delete_all_tags(soup, "head")
    _delete_all_tags(soup, "noscript")

    return soup.get_text()


def extract_content(html):

    """

        This method takes a raw HTML string ("<html><body><p>...") and
        tries to find the relevant textual content within it.

        The method uses serveral simple rules:

            - remove all scripts
            - remove all styles
            - remove all ...

            - simplify all a to their text
            - simplify all b to their text
            - simplify all ...

            - go trough all paragraphs and give points:
                - if it has a CLASS attribute that says "POST..." +25
                - if it has a ID attribute that says "POST..." +25
                - if it has a CLASS attribute that says "FOOTER..." -50
                - if it has a ID attribute that says "FOOTER..." -50

                - each "," +1

    """

    # prepare raw html string
    html = re.sub(BR, "</p><p>", html)

    soup = BeautifulSoup(html)

    soup = _simplify_html_before(soup)

    # get all paragraphs
    paragraphs = soup.findAll("p")
    topParent = None

    parents = []
    for paragraph in paragraphs:

        parent = paragraph.parent

        if parent not in parents:
            parents.append(parent)
            parent.score = 0

            # score using the attached class attribute if any
            if (parent.has_key("class")):
                if (NEGATIVE.match(str(parent["class"]))):
                    parent.score -= 50
                if (POSITIVE.match(str(parent["class"]))):
                    parent.score += 25

            if (parent.parent.has_key("class")):
                if (NEGATIVE.match(str(parent.parent["class"]))):
                    parent.score -= 50
                if (POSITIVE.match(str(parent.parent["class"]))):
                    parent.score += 25

            # score using the attached id attribute if any
            if (parent.has_key("id")):
                if (NEGATIVE.match(str(parent["id"]))):
                    parent.score -= 50
                if (POSITIVE.match(str(parent["id"]))):
                    parent.score += 25

        # init the score if it's still None
        if parent.score is None:
            parent.score = 0

        # score using a minimum length
        innerText = paragraph.renderContents()
        if (len(innerText) > 10):
            parent.score += 1

        # score using the number of ","
        parent.score += innerText.count(",")
        parent.score += innerText.count(".")

    for parent in parents:
        if ((not topParent) or (parent.score > topParent.score)):
            topParent = parent

    if (not topParent):
        return ""

    _delete_divs_with_less_content(topParent)
    _simplify_html_after(topParent)

    string = ""

    for element in topParent.findAll():
        string += "\n\n" + element.text.strip()

    #print topParent.renderContents()

    return string.strip()


def _simplify_html_after(soup):

    """
        This method simplifies the HTML and removes all attributes
        from the tags. E.g. <a href="..."> will be <a>

        Note: Use this method only after applying Arc90, since
        Arc90 needs the information in the class and id attribute!
    """

    for element in soup.findAll(True):

        element.attrs = {}

        if len(element.renderContents().strip()) == 0:
            element.extract()

    return soup


def _simplify_html_before(soup):

    """
        Prepares a soup object to be used for content extraction. This
        means that the HTML will be simplified pretty much. E.g. all a-tags
        will be replaced by their text. The resulting HTML is very simple
        and has a clear structure.

        Note: You can use this method before applying Arc90, it will not remove
        anything needed by the algorithm.
    """

    comments = soup.findAll(text=lambda text: isinstance(text, Comment))

    [comment.extract() for comment in comments]

    _tag_to_text(soup, "li", " - ")
    _tag_to_text(soup, "em")
    _tag_to_text(soup, "tt")
    _tag_to_text(soup, "b")
    _tag_to_text(soup, "strong")
    _tag_to_text(soup, "i")
    _tag_to_text(soup, "a")

    _replace_by_paragraph(soup, 'blockquote')
    _replace_by_paragraph(soup, 'quote')

    _delete_all_tags(soup, "link")
    _delete_all_tags(soup, "style")
    _delete_all_tags(soup, "script")
    _delete_all_tags(soup, "code")
    _delete_all_tags(soup, "pre")
    _delete_all_tags(soup, "form")
    _delete_all_tags(soup, "object")
    _delete_all_tags(soup, "iframe")
    _delete_all_tags(soup, "img")
    _delete_all_tags(soup, "embed")
    _delete_all_tags(soup, "head")
    _delete_all_tags(soup, "span")
    _delete_all_tags(soup, "noscript")

    _delete_if_no_text(soup, "td")
    _delete_if_no_text(soup, "tr")
    _delete_if_no_text(soup, "div")
    _delete_if_no_text(soup, "table")
    _delete_if_no_text(soup, "p")
    _delete_if_no_text(soup, "div")

    _delete_by_min_size(soup, "td", 10, 2)
    _delete_by_min_size(soup, "tr", 10, 2)
    _delete_by_min_size(soup, "div", 10, 2)
    _delete_by_min_size(soup, "table", 10, 2)
    _delete_by_min_size(soup, "p", 50, 2)

    return soup


def _delete_if_no_text(soup, tag):

    """
        Deletes all tags that have no content.
    """

    for p in soup.findAll(tag):
        if(len(p.renderContents().strip()) == 0):
            p.extract()


def _delete_by_min_size(soup, tag, length, children):

    """
        Deletes all tags that don't have a minimum size and
        a minimum amount of children.
    """

    for p in soup.findAll(tag):
        if(len(p.text) < length and len(p) <= children):
            p.extract()


def _replace_by_paragraph(soup, tag):

    """
        Replaces all tags of the given type by a paragraph tag
        with the same content, e.g. <a>...</a> will be <p>...</p>
    """

    for t in soup.findAll(tag):
        p = Tag(soup, name="p")
        t.replaceWith(p)
        p.insert(0, t.text)


def _delete_all_tags(soup, tag):

    """
        Delete all tags of the given type.
    """

    for t in soup.findAll(tag):
        t.extract()


def _tag_to_text(soup, tag, prefix=""):

    """
        Replaces all tags of the given types with their
        bare textual content.
    """

    for t in soup.findAll(tag):
        t.replaceWith(prefix + t.text.strip())


def _delete_divs_with_less_content(parent):

    """
        Removes all DIVs in the parent that have to less
        content. What is "to less"? Well, look in code.
        It's defined by some simple rules, like more images
        then paragraphs.
    """

    divs = parent.findAll("div")

    for div in divs:

        number_of_p = len(div.findAll("p"))
        number_of_img = len(div.findAll("img"))
        number_of_li = len(div.findAll("li"))
        number_of_a = len(div.findAll("a"))
        number_of_embed = len(div.findAll("embed"))
        number_of_pre = len(div.findAll("pre"))
        number_of_code = len(div.findAll("code"))

        if (div.renderContents().count(",") < 10):

            if ((number_of_pre == 0) and (number_of_code == 0)):

                if (
                        (number_of_img > number_of_p ) or
                        (number_of_li > number_of_p) or
                        (number_of_a > number_of_p) or
                        (number_of_p == 0) or
                        (number_of_embed > 0)
                    ):
                    div.extract()
