from time import mktime
import logging
from gensim import corpora
from gensim import models
from preprocess import create_corpus
import db
from config import Config
import logging


logging.basicConfig(format=Config.LOG_FORMAT, level=Config.LOG_LEVEL)


def save_topics(startDate, endDate, limit):

    startDate = mktime(startDate.timetuple())
    endDate = mktime(endDate.timetuple())

    topics = _get_topics(startDate, endDate, limit)

    db.insert_weekly_topics(startDate, endDate, topics)


def _get_topics(startDate, endDate, limit):

    links = db.fetch_links_by_date(startDate, endDate)
    entries = [doc['tokens'] for doc in create_corpus(links)]

    dictionary = corpora.Dictionary(entries)
    corpus = [dictionary.doc2bow(doc) for doc in entries]
    model = models.LsiModel(corpus, id2word=dictionary, num_topics=100, onepass=False)

    return model.show_topics(limit, formatted=False)
