import unittest
from preprocess import to_word_list


class Test(unittest.TestCase):

    def test_one_noun(self):

        words = to_word_list("this is an awesome class", True)

        processed = set(words)
        expected = set(["class"])

        difference = expected.difference(processed)

        self.failUnlessEqual(True, 0 == len(difference))

    def test_many_noun(self):

        words = to_word_list("this is an awesome class, a class like no other on this planet", True)

        processed = set(words)
        expected = set(["class", "planet"])

        difference = expected.difference(processed)

        self.failUnlessEqual(True, 0 == len(difference))

if __name__ == "__main__":
    unittest.main()
