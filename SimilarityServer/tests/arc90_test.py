import unittest
from os import path
from arc90 import extract_content


DATA_FOLDER = path.join(path.dirname(path.realpath(__file__)), 'test_data')


class Test(unittest.TestCase):

    def test_extract_content_with_Arc90_1(self):

        raw = path.join(DATA_FOLDER, 'raw_1.html')
        clean = path.join(DATA_FOLDER, 'clean_1.txt')

        processed = extract_content(self.get_file(raw))

        self.compare(self.get_file(clean), processed, 5)

    def test_extract_content_with_Arc90_2(self):

        raw = path.join(DATA_FOLDER, 'raw_2.html')
        clean = path.join(DATA_FOLDER, 'clean_2.txt')

        processed = extract_content(self.get_file(raw))

        self.compare(self.get_file(clean), processed, 5)

    def test_extract_content_with_Arc90_3(self):

        raw = path.join(DATA_FOLDER, 'raw_3.html')
        clean = path.join(DATA_FOLDER, 'clean_3.txt')

        processed = extract_content(self.get_file(raw))

        self.compare(self.get_file(clean), processed, 5)

    def test_extract_content_with_Arc90_4(self):

        raw = path.join(DATA_FOLDER, 'raw_4.html')
        clean = path.join(DATA_FOLDER, 'clean_4.txt')

        processed = extract_content(self.get_file(raw))

        self.compare(self.get_file(clean), processed, 5)

    def test_extract_content_with_Arc90_5(self):

        raw = path.join(DATA_FOLDER, 'raw_5.html')
        clean = path.join(DATA_FOLDER, 'clean_5.txt')

        processed = extract_content(self.get_file(raw))

        self.compare(self.get_file(clean), processed, 5)

    def get_file(self, file_name):

        with open(file_name, 'r') as file_to_read:
            content = file_to_read.read()

        return content.decode('utf-8')

    def compare(self, clean, processed, tolerance):

        clean_length = len(clean.split())
        processed_length = len(processed.split())

        clean_set = set(clean.split())
        processed_set = set(processed.split())

        difference = clean_set.difference(processed_set)

        self.failUnlessEqual(True, 0 <= len(difference) <= clean_length/tolerance)
        self.failUnlessEqual(True, clean_length - clean_length/tolerance < processed_length < clean_length + clean_length/tolerance)


if __name__ == "__main__":
    unittest.main()
