import unittest
from topics import get_topics, save_topics


class Test(unittest.TestCase):

    def test_get_topics(self):

        topics = get_topics("2013-05-08", "2013-05-09", 10)
        
        self.failUnlessEqual(10, len( topics ))

if __name__ == "__main__":
    unittest.main()