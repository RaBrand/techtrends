import sqlite3
import zlib
import logging

from contextlib import closing
from arc90 import extract_content
from preprocess import to_word_list


logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.DEBUG)
DB_FILE = '../VMLinkViewer/links.db'


def connect_db():
    connection = sqlite3.connect(DB_FILE)
    connection.row_factory = sqlite3.Row
    return connection


def decompress(buff):
    return zlib.decompress(buff).decode('utf-8')


def fetch_entries():
    query = 'SELECT id, html FROM links WHERE html IS NOT NULL'
    with closing(connect_db().cursor()) as cursor:
        rows = cursor.execute(query)
        corpus = []
        for row in rows:
            try:
                tokens = to_word_list(extract_content(decompress(row['html'])))
                if tokens:
                    corpus.append({
                        'id': row['id'],
                        'tokens': tokens
                    })
            except:
                logging.debug("can't create word list for link with id " + str( row[0] ))
        return corpus