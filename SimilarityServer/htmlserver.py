from flask import Flask
from db import get_html_for_linkid
import zlib


server = Flask(__name__)
# server.port = 5001


@server.route('/<int:id>')
def link(id):
        zipped_html = get_html_for_linkid(id)
        return zlib.decompress(zipped_html).decode('utf-8')
