import sqlite3
from contextlib import closing
from config import Config

QUANTIL = 98


def connect():
    connection = sqlite3.connect(Config.DB_FILE)
    connection.row_factory = sqlite3.Row
    connection.execute('PRAGMA foreign_keys = ON')
    connection.commit()
    return connection


def linkids_with_html():

    with closing(connect()) as db:

        cursor = db.cursor()

        query = 'SELECT id FROM links where HTML IS NOT NULL'
        rows = cursor.execute(query)

        return [row['id'] for row in rows]


def number_of_links():

    with closing(connect()) as db:

        cursor = db.cursor()

        query = 'SELECT count(*) FROM links WHERE html IS NOT NULL'

        rows = cursor.execute(query)

        return rows.fetchone()[0]


def fetch_link(id):

    with closing(connect()) as db:

        cursor = db.cursor()

        query = 'SELECT id, url, html FROM links WHERE id=:id'
        cursor.execute(query, {'id': id})

        return cursor.fetchone()


def fetch_links_by_date(start, end):

    query = '''
        SELECT links.id, links.html
        FROM links
        LEFT JOIN reddit ON links.id=reddit.url
        LEFT JOIN hackernews ON links.id=hackernews.url
        WHERE ((reddit.first > {start} AND reddit.last < {end}) OR
        (hackernews.first > {start} AND hackernews.last < {end})) AND
        links.html IS NOT NULL
        '''.format(start=start, end=end)

    with closing(connect()) as db:

        cursor = db.cursor()

        rows = cursor.execute(query)

        return [dict(row) for row in rows]


def fetch_links_with_votes(limit, offset=0):

    query = '''

        SELECT

            links.id AS id,
            links.url AS url,

            CASE WHEN hackernews.title NOT NULL
            THEN hackernews.title
            ELSE reddit.title
            END AS title,

            CASE WHEN hackernews.first NOT NULL
            THEN hackernews.first
            ELSE reddit.first
            END AS first,

            CASE WHEN hackernews.last NOT NULL
            THEN hackernews.last
            ELSE reddit.last
            END AS last,

            CASE WHEN hackernews.votes NOT NULL
            THEN hackernews.votes
            ELSE reddit.votes
            END AS votes,

            CASE WHEN hackernews.comments NOT NULL
            THEN hackernews.comments
            ELSE reddit.comments
            END AS comments,

            CASE WHEN hackernews.id NOT NULL
            THEN hackernews.id
            ELSE reddit.id
            END AS source

        FROM links

            LEFT JOIN reddit ON links.id=reddit.url
            LEFT JOIN hackernews ON links.id=hackernews.url

        WHERE (reddit.last IS NOT NULL or hackernews.last IS NOT NULL)
        AND (reddit.first IS NOT NULL or hackernews.first IS NOT NULL)

        ORDER BY links.id DESC

        LIMIT :limit

        OFFSET :offset
    '''

    with closing(connect()) as db:

        cursor = db.cursor()

        rows = cursor.execute(query, {'limit': limit, 'offset': offset})

        return [dict(row) for row in rows]


def fetch_links_by_date_with_votes(start, end, limit, offset=0):

    query = '''

        SELECT

            links.id AS id,
            links.url AS url,

            CASE WHEN hackernews.title NOT NULL
            THEN hackernews.title
            ELSE reddit.title
            END AS title,

            CASE WHEN hackernews.first NOT NULL
            THEN hackernews.first
            ELSE reddit.first
            END AS first,

            CASE WHEN hackernews.last NOT NULL
            THEN hackernews.last
            ELSE reddit.last
            END AS last,

            CASE WHEN hackernews.votes NOT NULL
            THEN hackernews.votes
            ELSE reddit.votes
            END AS votes,

            CASE WHEN hackernews.comments NOT NULL
            THEN hackernews.comments
            ELSE reddit.comments
            END AS comments,

            CASE WHEN hackernews.id NOT NULL
            THEN hackernews.id
            ELSE reddit.id
            END AS source

        FROM links

            LEFT JOIN reddit ON links.id=reddit.url
            LEFT JOIN hackernews ON links.id=hackernews.url

        WHERE ((reddit.first > :start AND reddit.last < :end)
        OR (hackernews.first > :start AND hackernews.last < :end))

        ORDER BY links.id DESC

        LIMIT :limit

        OFFSET :offset
    '''

    with closing(connect()) as db:

        cursor = db.cursor()

        rows = cursor.execute(query, {'start': start, 'end': end, 'limit': limit, 'offset': offset})

        return [dict(row) for row in rows]


def fetch_links_by_ids(ids):

    #it's okay to use no prepared statement here,
    #because the ids are no user input

    query = '''

        SELECT

            links.id AS id,
            links.url AS url,

            CASE WHEN hackernews.title NOT NULL
            THEN hackernews.title
            ELSE reddit.title
            END AS title,

            CASE WHEN hackernews.first NOT NULL
            THEN hackernews.first
            ELSE reddit.first
            END AS first,

            CASE WHEN hackernews.last NOT NULL
            THEN hackernews.last
            ELSE reddit.last
            END AS last,

            CASE WHEN hackernews.votes NOT NULL
            THEN hackernews.votes
            ELSE reddit.votes
            END AS votes,

            CASE WHEN hackernews.comments NOT NULL
            THEN hackernews.comments
            ELSE reddit.comments
            END AS comments,

            CASE WHEN hackernews.comments NOT NULL
            THEN ((
            min(
            (
                (hackernews.votes /
                (SELECT votes_var FROM avg_var WHERE type="hackernews") /
                (SELECT votes_avg FROM avg_var WHERE type="hackernews")) /
                (SELECT max_votes FROM max_vals WHERE type="hackernews")
            ), 1) +

            min(
            (
                (hackernews.comments /
                (SELECT comments_var FROM avg_var WHERE type="hackernews") /
                (SELECT comments_avg FROM avg_var WHERE type="hackernews")) /
                (SELECT max_comments FROM max_vals WHERE type="hackernews")
            ), 1) +

            min(
            (
                ((hackernews.last - hackernews.first) /
                (SELECT duration_var FROM avg_var WHERE type="hackernews") /
                (SELECT duration_avg FROM avg_var WHERE type="hackernews")) /
                (SELECT max_duration FROM max_vals WHERE type="hackernews")
            ), 1))
            / 3) * 100
            ELSE ((
            min(
            (
                (reddit.votes /
                (SELECT votes_var FROM avg_var WHERE type="reddit") /
                (SELECT votes_avg FROM avg_var WHERE type="reddit")) /
                (SELECT max_votes FROM max_vals WHERE type="reddit")
            ), 1) +

            min(
            (
                (reddit.comments /
                (SELECT comments_var FROM avg_var WHERE type="reddit") /
                (SELECT comments_avg FROM avg_var WHERE type="reddit")) /
                (SELECT max_comments FROM max_vals WHERE type="reddit")
            ), 1) +

            min(
            (
                ((reddit.last - reddit.first) /
                (SELECT duration_var FROM avg_var WHERE type="reddit") /
                (SELECT duration_avg FROM avg_var WHERE type="reddit")) /
                (SELECT max_duration FROM max_vals WHERE type="reddit")
            ), 1))
            / 3) * 100
            END AS popularity,

            CASE WHEN hackernews.id NOT NULL
            THEN hackernews.id
            ELSE reddit.id
            END AS source

        FROM links

            LEFT JOIN reddit ON links.id=reddit.url
            LEFT JOIN hackernews ON links.id=hackernews.url

        WHERE links.id IN ({ids})
        AND (reddit.last IS NOT NULL or hackernews.last IS NOT NULL)
        AND (reddit.first IS NOT NULL or hackernews.first IS NOT NULL)

        ORDER BY links.id DESC

    '''.format(ids=', '.join(ids))

    with closing(connect()) as db:

        cursor = db.cursor()

        rows = cursor.execute(query)

        return rows.fetchall()


def fetch_links_by_ids_and_date(ids, start, end):

    query = '''

        SELECT

            links.id AS id,
            links.url AS url,

            CASE WHEN hackernews.title NOT NULL
            THEN hackernews.title
            ELSE reddit.title
            END AS title,

            CASE WHEN hackernews.first NOT NULL
            THEN hackernews.first
            ELSE reddit.first
            END AS first,

            CASE WHEN hackernews.last NOT NULL
            THEN hackernews.last
            ELSE reddit.last
            END AS last,

            CASE WHEN hackernews.votes NOT NULL
            THEN hackernews.votes
            ELSE reddit.votes
            END AS votes,

            CASE WHEN hackernews.comments NOT NULL
            THEN hackernews.comments
            ELSE reddit.comments
            END AS comments,

            CASE WHEN hackernews.comments NOT NULL
            THEN ((
            min(
            (
                (hackernews.votes /
                (SELECT votes_var FROM avg_var WHERE type="hackernews") /
                (SELECT votes_avg FROM avg_var WHERE type="hackernews")) /
                (SELECT max_votes FROM max_vals WHERE type="hackernews")
            ), 1) +

            min(
            (
                (hackernews.comments /
                (SELECT comments_var FROM avg_var WHERE type="hackernews") /
                (SELECT comments_avg FROM avg_var WHERE type="hackernews")) /
                (SELECT max_comments FROM max_vals WHERE type="hackernews")
            ), 1) +

            min(
            (
                ((hackernews.last - hackernews.first) /
                (SELECT duration_var FROM avg_var WHERE type="hackernews") /
                (SELECT duration_avg FROM avg_var WHERE type="hackernews")) /
                (SELECT max_duration FROM max_vals WHERE type="hackernews")
            ), 1))
            / 3) * 100
            ELSE ((
            min(
            (
                (reddit.votes /
                (SELECT votes_var FROM avg_var WHERE type="reddit") /
                (SELECT votes_avg FROM avg_var WHERE type="reddit")) /
                (SELECT max_votes FROM max_vals WHERE type="reddit")
            ), 1) +

            min(
            (
                (reddit.comments /
                (SELECT comments_var FROM avg_var WHERE type="reddit") /
                (SELECT comments_avg FROM avg_var WHERE type="reddit")) /
                (SELECT max_comments FROM max_vals WHERE type="reddit")
            ), 1) +

            min(
            (
                ((reddit.last - reddit.first) /
                (SELECT duration_var FROM avg_var WHERE type="reddit") /
                (SELECT duration_avg FROM avg_var WHERE type="reddit")) /
                (SELECT max_duration FROM max_vals WHERE type="reddit")
            ), 1))
            / 3) * 100
            END AS popularity,

            CASE WHEN hackernews.id NOT NULL
            THEN hackernews.id
            ELSE reddit.id
            END AS source

        FROM links

            LEFT JOIN reddit ON links.id=reddit.url
            LEFT JOIN hackernews ON links.id=hackernews.url

        WHERE links.id IN ({ids})
        AND ((reddit.first > {start} AND reddit.last < {end})
        OR (hackernews.first > {start} AND hackernews.last < {end}))

        ORDER BY links.id DESC

    '''.format(ids=', '.join(ids), start=start, end=end)

    with closing(connect()) as db:

        cursor = db.cursor()

        rows = cursor.execute(query)

        return rows.fetchall()


def _insert_links(cursor, links):

        cursor.executemany(
            'INSERT OR IGNORE INTO links (html, url) VALUES (:html, :url)', links
        )


def update_links(links):
    """Updates the given links' equivalents in the db

        Expects the links that should be updated as a dictionary
        with all columns the link db schema contains.
        Uses the SQL UPDATE statement to update entries with
        an exact match of id and url property.
    """
    with closing(connect()) as db:

        cursor = db.cursor()

        cursor.executemany(
            'UPDATE links SET html=:html WHERE id=:id AND url=:url', links
        )
        db.commit()


def _set_hackernews_avg_var(cursor):

        cursor.execute(
            '''INSERT OR REPLACE INTO avg_var
               VALUES(
               "hackernews",
               (SELECT avg(votes) FROM hackernews),
               (SELECT avg((hackernews.votes-sub.a)*(hackernews.votes-sub.a))
                    AS var FROM hackernews,
                    (SELECT avg(votes) AS a FROM hackernews) AS sub),
               (SELECT avg(comments) FROM hackernews),
               (SELECT avg((hackernews.comments-sub.a)*(hackernews.comments-sub.a))
                    AS var FROM hackernews,
                    (SELECT AVG(comments) AS a FROM hackernews) AS sub),
               (SELECT avg(dur) FROM (SELECT (last - first) as dur FROM hackernews)),
               (SELECT avg(((last -  first)-sub.a)*((last -  first)-sub.a))
                    AS var from hackernews,
                    (SELECT avg(last -  first) AS a FROM hackernews) AS sub)
               )
            '''
        )

        max_votes_query = '''SELECT votes
                                / (SELECT votes_var FROM avg_var WHERE type="hackernews")
                                / (SELECT votes_avg FROM avg_var WHERE type="hackernews")
                             AS std_votes
                             FROM hackernews ORDER BY std_votes'''
        cursor.execute(max_votes_query)
        votes = cursor.fetchall()
        max_vote = votes[(len(votes) / 100) * QUANTIL][0]

        max_comments_query = '''SELECT comments
                                    / (SELECT comments_var FROM avg_var WHERE type="hackernews")
                                    / (SELECT comments_avg FROM avg_var WHERE type="hackernews")
                                AS std_comments
                                FROM hackernews ORDER BY std_comments'''
        cursor.execute(max_comments_query)
        comments = cursor.fetchall()
        max_comment = comments[(len(comments) / 100) * QUANTIL][0]

        max_duration_query = '''SELECT (last - first)
                                    / (SELECT duration_var FROM avg_var WHERE type="hackernews")
                                    / (SELECT duration_avg FROM avg_var WHERE type="hackernews")
                                 AS std_duration
                                FROM hackernews ORDER BY std_duration'''
        cursor.execute(max_duration_query)
        durations = cursor.fetchall()
        max_duration = durations[(len(durations) / 100) * QUANTIL][0]

        cursor.execute('''
            INSERT OR REPLACE INTO max_vals
            VALUES("hackernews", :max_vote, :max_comment, :max_duration)
        ''', {'max_vote': max_vote, 'max_comment': max_comment, 'max_duration': max_duration})




def insert_hackernews_posts(posts):
    """Insert new links into database

        Expects a list of dictionaries, where every post is a dict.
        If the link already exists inside the db, it will be updated.
    """

    with closing(connect()) as db:

        cursor = db.cursor()

        _insert_links(cursor, posts)

        cursor.executemany(
            '''INSERT OR REPLACE INTO hackernews
            (id, first, last, title, votes, comments, url)
            VALUES (:unique_id,
            COALESCE(
                (SELECT first from hackernews WHERE id=:unique_id), :first),
            :last, :title, :votes, :comments,
            (SELECT id FROM links WHERE url=:url))''', posts
        )

        _set_hackernews_avg_var(cursor)

        db.commit()


def _set_reddit_avg_var(cursor):

        cursor.execute(
            '''INSERT OR REPLACE INTO avg_var
               VALUES(
               "reddit",
               (SELECT avg(votes) FROM reddit),
               (SELECT avg((reddit.votes-sub.a)*(reddit.votes-sub.a))
                    AS var FROM reddit,
                    (SELECT avg(votes) AS a FROM reddit) AS sub),
               (SELECT avg(comments) FROM reddit),
               (SELECT avg((reddit.comments-sub.a)*(reddit.comments-sub.a))
                    AS var FROM reddit,
                    (SELECT AVG(comments) AS a FROM reddit) AS sub),
               (SELECT avg(dur) FROM (SELECT (last - first) as dur FROM reddit)),
               (SELECT avg(((last -  first)-sub.a)*((last -  first)-sub.a))
                    AS var from reddit,
                    (SELECT avg(last -  first) AS a FROM reddit) AS sub)
               )
            '''
        )

        max_votes_query = '''SELECT votes
                                / (SELECT votes_var FROM avg_var WHERE type="reddit")
                                / (SELECT votes_avg FROM avg_var WHERE type="reddit")
                             AS std_votes
                             FROM reddit ORDER BY std_votes'''
        cursor.execute(max_votes_query)
        votes = cursor.fetchall()
        max_vote = votes[(len(votes) / 100) * QUANTIL][0]

        max_comments_query = '''SELECT comments
                                    / (SELECT comments_var FROM avg_var WHERE type="reddit")
                                    / (SELECT comments_avg FROM avg_var WHERE type="reddit")
                                AS std_comments
                                FROM reddit ORDER BY std_comments'''
        cursor.execute(max_comments_query)
        comments = cursor.fetchall()
        max_comment = comments[(len(comments) / 100) * QUANTIL][0]

        max_duration_query = '''SELECT (last - first)
                                    / (SELECT duration_var FROM avg_var WHERE type="reddit")
                                    / (SELECT duration_avg FROM avg_var WHERE type="reddit")
                                 AS std_duration
                                FROM reddit ORDER BY std_duration'''
        cursor.execute(max_duration_query)
        durations = cursor.fetchall()
        max_duration = durations[(len(durations) / 100) * QUANTIL][0]

        cursor.execute('''
            INSERT OR REPLACE INTO max_vals
            VALUES("reddit", :max_vote, :max_comment, :max_duration)
        ''', {'max_vote': max_vote, 'max_comment': max_comment, 'max_duration': max_duration})


def insert_reddit_posts(posts):
    """Insert new links into database

        Expects a list of dictionaries, where every post is a dict.
        If the link already exists inside the db, it will be updated.
    """

    with closing(connect()) as db:

        cursor = db.cursor()

        _insert_links(cursor, posts)

        cursor.executemany(
            '''INSERT OR REPLACE INTO reddit
            (id, first, last, title, votes, comments, url)
            VALUES (:unique_id,
            coalesce((select first from reddit where id=:unique_id), :first),
            :last, :title, :votes, :comments,
            (SELECT id FROM links WHERE url=:url))''', posts
        )
        _set_reddit_avg_var(cursor)

        db.commit()


def get_html_for_linkid(id):
    with closing(connect()) as db:
        cursor = db.execute('SELECT html FROM links WHERE id=:id', {'id': id})
        return cursor.fetchone()[0]


def get_links_where_html_absent():
    """Returns all links with no html content

        Returns all links with no html content as a list of tuples.
        The tuple contains all columns specified by the links db schema.
    """

    with closing(connect()) as db:
        cursor = db.execute('SELECT * FROM links WHERE html is NULL')
        return cursor.fetchall()
