from SimilarityServer import train_server
from config import Config
import logging
from SimilarityServer import html_server
from multiprocessing import Process
import os


logging.basicConfig(format=Config.LOG_FORMAT, level=Config.LOG_LEVEL)


if __name__ == '__main__':

    process = Process(target=html_server.run, kwargs={'port': 5001})
    process.start()

    train_server()

    os.kill(process.pid, 9)
